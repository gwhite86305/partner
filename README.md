# Partner - Flask Application
---

A python-flask application for performing CRUD operations on a business Partner database.

Uses: Postgresql as the dialect for SQLALchemy.
      POSTMAN for HTTP requests to/from the server to validate the API's


## Setup
---

1.  git clone git@bitbucket.org:gwhite86305/partner.git

    pip install virtualenv

    virtualenv env

    source env/bin/activate

    pip install -r requirements.txt

2.  Install and setup Postman

    TBD     

3. Install and setup MySQL 

    TBD   

3.  The following will start a flask-server on `localhost:5000`
    
     
    python run.py

4. Open a second terminal window:

4.  Server is running on http://localhost:5000` 

5.  Implements the following API's

   https://127.0.0.1:5000/api/partners/

   https://127.0.0.1:5000/api/partners/<int:id>)

   https://127.0.0.1:5000:/api/users/

   https://127.0.0.1:5000/api/users/<int:id>)
  
