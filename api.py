from flask import Flask
from flask_restful import abort, Api, fields, marshal_with, reqparse, Resource
from datetime import datetime
from models import MessageModel
import status
from pytz import utc

class PartnerManager():
    last_id = 0
    def __init__(self):
        self.names = {}

    def insert_partner(self, name):
        self.__class__  .last_id += 1
        message.id = self.__class__.last_id
        self.names[self.__class__.last_id] = name

    def get_partner(self, id):
        return self.names[id]

    def delete_partner(self, id):
        del self.names[id]

partner_fields = {
    'id': fields.Integer,
    'uri': fields.Url('partner_endpoint'),
    'name': fields.String,
    'key': fields.String
}   

partner_manager = PartnerManager()

class Partner(Resource):
    def abort_if_name_doesnt_exist(self, id):
        if id not in partner_manager.names:
            abort(
                status.HTTP_404_NOT_FOUND,
                message="Name {0} doesn't exist".format(id))

    @marshal_with(partner_fields)
    def get(self, id):
        self.abort_if_name_doesnt_exist(id)
        return partner_manager.get_partner(id)

    def delete(self, id):
        self.abort_if_name_doesnt_exist(id)
        partner_manager.delete_partner(id)
        return '', status.HTTP_204_NO_CONTENT

    @marshal_with(partner_fields)
    def patch(self, id):
        self.abort_if_name_doesnt_exist(id)
        partner = partner_manager.get_partner(id)
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str)
        parser.add_argument('key', type=str)
        args = parser.parse_args()
        if 'name' in args:
            partner.name = args['name']
        if 'key' in args:
            partner.key = args['key']
        return partner

class PartnerList(Resource):
    @marshal_with(partner_fields)
    def get(self):
        return [v for v in partner_manager.names.values()]

    @marshal_with(partner_fields)
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, required=True, help='Partner Name cannot be blank!')
        parser.add_argument('key', type=str, required=True, help='Key cannot be blank!')
        args = parser.parse_args()
        partner = PartnerModel(
            name=args['name'],
            key=args['key'],
            )
        partner_manager.insert_partner(name)
        return partner, status.HTTP_201_CREATED

app = Flask(__name__)
api = Api(app)
api.add_resource(PartnerList, '/api/partners/')
api.add_resource(Partner, '/api/partners/<int:id>', endpoint='partner_endpoint')


if __name__ == '__main__':
    app.run(debug=True)
