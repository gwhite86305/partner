from flask import Blueprint, request, jsonify, make_response
from flask_restful import Api, Resource
from models import db, Partner, PartnerSchema
from sqlalchemy.exc import SQLAlchemyError
import status
from helpers import PaginationHelper
from flask_httpauth import HTTPBasicAuth
from flask import g
from models import User, UserSchema


auth = HTTPBasicAuth()

@auth.verify_password
def verify_user_password(name, password):
    user = User.query.filter_by(name=name).first()
    if not user or not user.verify_password(password):
        return False
    g.user = user
    return True

class AuthRequiredResource(Resource):
    method_decorators = [auth.login_required]

api_bp = Blueprint('api', __name__)
partner_schema = PartnerSchema()
user_schema = UserSchema()
api = Api(api_bp)

class UserResource(AuthRequiredResource):
    def get(self, id):
        user = User.query.get_or_404(id)
        result = user_schema.dump(user).data
        return result

class UserListResource(Resource):
    @auth.login_required
    def get(self):
        pagination_helper = PaginationHelper(
            request,
            query=User.query,
            resource_for_url='api.userlistresource',
            key_name='results',
            schema=user_schema)
        result = pagination_helper.paginate_query()
        return result

    def post(self):
        request_dict = request.get_json()
        if not request_dict:
            response = {'user': 'No input data provided'}
            return response, status.HTTP_400_BAD_REQUEST
        errors = user_schema.validate(request_dict)
        if errors:
            return errors, status.HTTP_400_BAD_REQUEST
        name = request_dict['name']
        existing_user = User.query.filter_by(name=name).first()
        if existing_user is not None:
            response = {'user': 'An user with the same name already exists'}
            return response, status.HTTP_400_BAD_REQUEST
        try:
            user = User(name=name)
            error_message, password_ok = \
                user.check_password_strength_and_hash_if_ok(request_dict['password'])
            if password_ok:
                user.add(user)
                query = User.query.get(user.id)
                result = user_schema.dump(query).data
                return result, status.HTTP_201_CREATED
            else:
                return {"error": error_message}, status.HTTP_400_BAD_REQUEST
        except SQLAlchemyError as e:
            db.session.rollback()
            resp = {"error": str(e)}
            return resp, status.HTTP_400_BAD_REQUEST

class PartnerResource(AuthRequiredResource):
    def get(self, id):
        name = Partner.query.get_or_404(id)
        result = partner_schema.dump(name).data
        return result

    def patch(self, id):
        partner = Partner.query.get_or_404(id)
        partner_dict = request.get_json(force=True)
        if 'name' in partner_dict:
            partner_name = partner_dict['name']
            if Partner.is_unique(id=id, name=partner_name):
                partner.name = partner_name
            else:
                response = {'error': 'A partner  with the same name already exists'}
                return response, status.HTTP_400_BAD_REQUEST
        if 'key' in partner_dict:
            partner.key = partner_dict['key']
        dumped_partner, dump_errors = partner_schema.dump(partner)
        if dump_errors:
            return dump_errors, status.HTTP_400_BAD_REQUEST
        validate_errors = partner_schema.validate(dumped_partner)
        if validate_errors:
            return validate_errors, status.HTTP_400_BAD_REQUEST
        try:
            partner.update()
            return self.get(id)
        except SQLAlchemyError as e:
                db.session.rollback()
                resp = {"error": str(e)}
                return resp, status.HTTP_400_BAD_REQUEST

    def delete(self, id):
        partner = Partner.query.get_or_404(id)
        try:
            delete = partner.delete(partner)
            response = make_response()
            return response, status.HTTP_204_NO_CONTENT
        except SQLAlchemyError as e:
                db.session.rollback()
                resp = jsonify({"error": str(e)})
                return resp, status.HTTP_401_UNAUTHORIZED


class PartnerListResource(AuthRequiredResource):
    def get(self):
        pagination_helper = PaginationHelper(
            request,
            query=Partner.query,
            resource_for_url='api.partnerlistresource',
            key_name='results',
            schema=partner_schema)
        result = pagination_helper.paginate_query()
        return result

    def post(self):
        request_dict = request.get_json()
        if not request_dict:
            response = {'partner': 'No input data provided'}
            return response, status.HTTP_400_BAD_REQUEST
        errors = partner_schema.validate(request_dict)
        if errors:
            return errors, status.HTTP_400_BAD_REQUEST
        partner_name = request_dict['name']
        if not Partner.is_unique(id=0, partner=partner_name):
            response = {'error': 'A partner with the same name already exists'}
            return response, status.HTTP_400_BAD_REQUEST
        try:
            partner = Partner(
                name=partner_name,
                key=request_dict['key'])
            partner.add(partner)
            query = Partner.query.get(partner.id)
            result = partner_schema.dump(query).data
            return result, status.HTTP_201_CREATED
        except SQLAlchemyError as e:
            db.session.rollback()
            resp = {"error": str(e)}

api.add_resource(PartnerListResource, '/partners/')
api.add_resource(PartnerResource, '/partners/<int:id>')
api.add_resource(UserListResource, '/users/')
api.add_resource(UserResource, '/users/<int:id>')
